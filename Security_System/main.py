'''
Todo: 

We are using the envelope method now with python-gnupg and not pretty-bad-protocol. Envelope does not work with pretty-bad-protocol. I needed a wrapper for MIME emails. I am certain that the dark arcane magics of mime email structure will remain locked to me forever. Mostly because I couldn't figure out how to add a signature...
'''

import time, subprocess, os, smtplib, requests
import RPi.GPIO as io
import gnupg

from envelope import Envelope

from requests.auth import HTTPBasicAuth

# Import GPG stuff.
# Replace me with the home dir of whatever user is running the script. 
gpg = gnupg.GPG(gnupghome='/home/username/.gnupg')
strFrom = 'someone@someplace.com' # Replaced for example
strTo = 'someone@someplace.com'     # Replaced for example
recipient_pub_key = 'pubkeyFingerPrint' # this is the output of gpg --keyid-format long --list-keys

imageDir = "/home/username/securitySystem/motion-images/"

io.setmode(io.BCM)
pir_sensor_pin = 23     # PIR Motion Sensor Pin

#doorStillOpen = 0;       # Bool for doors still open.
motion_shutdown_bool = 0 # Boot for motion process kill.

# Image File
current_image = ""

# Setup th GPIO Inputs, using th Pin variables above.
io.setup(pir_sensor_pin, io.IN)

def send_message(body):
    try:
    # Send the email via our own SMTP server.
        s = smtplib.SMTP('localhost')
        s.sendmail(strFrom, strTo, body)
        s.quit()
    except Exception as error:
        print (error)

# Capture the image and then call the compose message, and subsequently the encryption method.
def capture_image():
    try:
        timestr = time.strftime("%Y%m%d-%H%M%S")
        print(timestr)
        # Todo put all this in a conf file. 
        # Maybe use a USB camera. 
        r = requests.get("http://IPCamera/image.jpg", auth=HTTPBasicAuth("replaceMe","WithCredentialsForCamera"))
        open(imageDir+timestr+".jpg", 'wb').write(r.content)
        print(timestr)
        current_image = imageDir+timestr+".jpg"
        print(current_image)

        Envelope("Motion Detected: ").subject("Motion Detected").to(strTo).sender(strFrom).attach(path=current_image, inline=True).smtp("localhost").encryption().send()

    except Exception as error:
        print(error)


while True:
    i=io.input(23)
    if i==0:                 #When output from motion sensor is LOW
          # DO NOTHING.
          time.sleep(0.1)
    elif i==1:               #When output from motion sensor is HIGH
          #TAKE PICTURE
          capture_image()
          time.sleep(100)


