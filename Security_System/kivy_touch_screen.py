# import the required libraries. 
import kivy
kivy.require('1.9.2')
import subprocess
import os

from kivy.uix.gridlayout import GridLayout
from kivy.app import App
from kivy.uix.button import Button

# Define Kivy APP class. 
class MyApp(App):
    # Defne functions that perform tasks by calling shell scripts. 
    def startMotion(self):
        subprocess.call(['/home/pi/myKivy/startMoCap.sh'])
    def stopMotion(self):
        subprocess.call(['/home/pi/myKivy/stopMoCap.sh'])
    def startAlarm(self):
        subprocess.call(['/home/pi/myKivy/startAlarm.sh'])
    def stopAlarm(self):
        subprocess.call(['/home/pi/myKivy/stopAlarm.sh'])
#    def playStuff(self):
#        subprocess.call(['omxplayer', './NOW.mp3'])

    def build(self):
        layout = GridLayout(cols=3)
        # Button that will start the motion service. Old Code, not needed. 
        btn1 = Button(text='Start Motion')
        # Button that will stop the motion service. Ditto
        btn2 = Button(text='Stop Motion')
        # Button that will start the alarm, AKA main.py
        btn3 = Button(text='Start Alarm')
        # Button that will stop the alarm, AKA kill main.py. 
        btn4 = Button(text='Stop Alarm')
        # ????
        btn5 = Button(text='Place Holder')
        # Button that will quit the touch screen app.
        btn6 = Button(text='Quit')
        # Add the buttons and define their function. 
        layout.add_widget(btn1)
        btn1.bind(on_press=lambda App:self.startMotion())
        layout.add_widget(btn2)
        btn2.bind(on_press=lambda App:self.stopMotion())
        layout.add_widget(btn3)
        btn3.bind(on_press=lambda App:self.startAlarm())
        layout.add_widget(btn4)
        btn4.bind(on_press=lambda App:self.stopAlarm())
        layout.add_widget(btn5)
#        btn5.bind(on_press=lambda App:self.playStuff())       
        layout.add_widget(btn6)
        btn6.bind(on_press=lambda App:self.get_running_app().stop())
        return layout


if __name__ in ('__android__', '__main__'):
    MyApp().run()


