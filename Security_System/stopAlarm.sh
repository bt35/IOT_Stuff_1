#!/bin/bash
# This script stops the alarm
# It is mapped to the touch screen application which is defined 
# in kivy_touch_screen.py. 

# Stop the python alarm script by its specific name, so as to not stop any other 
# python process which could stop the kivy application that runs the touch screen.

sudo pkill -f anotherDoor.py

# Send an email using the MTA system on the raspberry pi. The email will contain all python processes that are still 
# running and should not show the python alarm script called anotherDoor.py 
ps aux | grep python | mail -s "Alarm Stopped" xxx@gmail.com

# Remove the python alarm script lock directory. 
trap 'rm -rf /tmp/sec.lock' EXIT

sleep 15

exit

