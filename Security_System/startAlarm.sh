#!/bin/bash
# This script starts the alarm
# It is mapped to the touch screen application which is defined 
# in kivy_touch_screen.py.  

# Create a lock directory for the alarm process. 
if ! mkdir /tmp/sec.lock; then
    printf "Failed to aquire lock.\n" >&2
    exit 1
fi

# Start the alarm python script and disown the  process. 
sudo python /home/pi/anotherDoor.py &

# Send an email using the MTA on the raspberry pi. 
ps aux | grep python | mail -s "Alarm Started" xxxx.xxx.@gmail.com 
sleep 15
exit
