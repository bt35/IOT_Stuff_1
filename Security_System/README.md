# Raspberry Pi 2 B+ Touch Screen Security System

## Attributions 
I wrote this code a long while ago and I cannot recall which tutorials I may have used. Most of the code was written based off prior knowledge or commonly used techniques. However if any code resembles a tutorial closely please let me know so that I can attribute it. 

Shout out to the Kivy Maintainers.

Thanks to Python-gnupg maintaners, and python-gpgmime maintaners. 
https://github.com/isislovecruft/python-gnupg
https://github.com/zenhack/python-gpgmime

## What is it? 
This is by far the most advanced project I have attempted and it is not even complete. I suspect that it may never be complete due to a laziness when it comes to running cable with a fish tape. 

The purpose of this project is to monitor a certain area of a house with a camera and send emails when a PIR sensor is tripped. But also there are magnetic reed switches on certain portals that will also trigger an email should they open. 

When this code was first written I was using the [Motion Serivice](http://www.lavrsen.dk/foswiki/bin/view/Motion/) to great effect. However it soon became apparent that I would get emails due to light blooms no matter what thresholds were set. Motion detects the differences in pictures and it eliminates the need for motion sensors and extra code and it works very well. However in my case there was a light source that will always bloom during the day time. If you have checked some of my other projects you will know that I keep a few plants and that is the cause of the light problem. So I purchased some small PIR motion sensors and to date have only had 2 false positive emails. 

I almost forgot the most important part... the camera. This system utilizes a Lynksis camera that is connected via Ethernet, cause wireless is very unstable these days due to signal saturation. When the system detects motion it uses Curl to grab the image from the video feed. See the code for comments. 

## What's new? 
The emails that are sent containing pictures can now be encrypted using the recipient public key. 

Pair this together with Openkeychain, and K9 Mail which works with the existing gmail functionality. This means the image emails are stored as encrypted in your gmail inbox until you decrpypt them. 

## How's the Email Stuff work? 
This system uses postfix configured to send email using Gmail's SMTP server. A tutorial like this can help: https://www.linode.com/docs/email/postfix/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu/. 


## Materials 
- 1 x Raspberry Pi 2 B+
- 1 x Assembled Pi Cobbler Plus
- 1 x GPIO Ribbon Cable for Raspberry Pi Model A+/B+/Pi 2 (40 pins)
- 1 x EMY 5 X HC-SR501 Adjust Ir Pyroelectric Infrared PIR Motion Sensor Detector Modules 
- 7 Inch Official Raspberry Pi Touch Screen
- Surface Mount Magnetic Contacts (AKA Magnetic Door Sensors, or Reed Switches)
- Red and White Bell Wire - Door Bell Wire
- Jumpers

## Pin Out
- Door Sensor GPIO Pin: 21          
- Door Sensor GPIO Pin: 20
- Magnetic switch to turn motion sensor off GPIO Pin: 25 
- Pir Sensor GPIO Pin: 23     
- A wired or wireless camera. 

##Notes: 
Kivy 1.9.2 is required for the touch screen to work. However with some modifications this system can be controlled by the shell scripts, and even a rudimentary Apache server.

## Images 
![alt text](/Security_System/20170627_025339.jpg)
![alt text](/Security_System/20170627_025400.jpg)

## TODO: 

Add two threads to the main loop. One thread should handle the motion sensings. Another thread should handle doors, also need to handle multiple doors at the same time! 

