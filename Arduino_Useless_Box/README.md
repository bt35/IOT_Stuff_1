# Arduino Motion Sensor - Useless Box 

## Attributions 
I wrote this code a long while ago and I cannot recall which tutorials I may have used. The code is also a hodge podge of stuff taken from other sketches. 

Adafruit tutorial and libraries are used here. 

## What is it? 

It is a completely unnecessary use of Arduino, scavenged LED and resistors and an Adafruit temperature/humidity sensor. 

This repo accomplishes two things, one backs up a project which I am going to scavenge parts from, and provides a useless project for others to attempt. (If anyone reads these >.>). 

## Materials

- Adafruit Temp & Humidity Module: Si7021
	https://learn.adafruit.com/adafruit-si7021-temperature-plus-humidity-sensor/wiring-and-test
- Arduino Uno Board
- Small solderless connection board. Breadboard. 
- 1 x EMY 5 X HC-SR501 Adjust Ir Pyroelectric Infrared PIR Motion Sensor Detector Modules 
- A bunch of jumpers.
- I2C 1602 LCD-2 Line, 16 character LCD with back light. 
	http://www.hessmer.org/blog/2014/01/11/arduino-compatible-iic-i2c-serial-2-5-lcd-1602-display-module/
- Server Faceplate that has 4 LEDs a built in resistor. 

## Pinouts 
- Grounds -> Create two ground rails on the breadboard by using two ground pin outs.
- Power -> Create one power rail on the breadboard by using the 5v output on the Arduino Uno. 
- Si7021 - Adafruit Temperature & Humidity Sensor
	- SDA -> Digital 20 - i2C Data pin on Arduino Uno.
	- SCL -> Digital 21 - i2C Clock pin on Arduino Uno.  
	- Ground -> ground rail on breadboard. 
	- Power off of a 5v power rail on the bread board. 
- PIR Motion Sensor
	- Power pin is connected to the 5v power rail on the bread board. 
	- Ground pin is connected to the ground rail on the bread board. 
	- Output connected to Digital 2. 
- LCD
        - Power pin is connected to the 5v power rail on the bread board.
        - Ground pin is connected to the ground rail on the bread board.
	- SDA -> A4
	- SCL -> A5
- LEDs on the Server Faceplate
	- Grounds Connect to a ground rail on the Arduino Uno
	- Power status LED Pin -> 13
	- Motion sensor status LED Pin -> 12

### Pinout Picture
## Images 
![alt text](/Arduino_Useless_Box/20170912_042031.jpg)

### Gif of Action
![alt_text](/Arduino_Useless_Box/20170912_041917.mp4.gif/)


