#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_Si7021.h>
#include <stdio.h>
 
#define I2C_ADDR 0x27
#define BACKLIGHT_PIN 3
#define En_pin 2
#define Rw_pin 1
#define Rs_pin 0
#define D4_pin 4
#define D5_pin 5
#define D6_pin 6
#define D7_pin 7
#define MAX_OUT_CHARS 16 
 
LiquidCrystal_I2C lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin,BACKLIGHT_PIN,POSITIVE);
LCD *myLCD = &lcd;

const int ledPin = 13;                // choose the pin for the LED
const int runningLED = 12;
const unsigned long runningLedInterval = 1000;
unsigned long runningLedTimer;

int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status
int moCount = 0;

// Temperature Sensor
Adafruit_Si7021 sensor = Adafruit_Si7021();

void setup(){
  sensor.begin();
  lcd.begin(16,2);               // initialize the lcd
  lcd.setCursor(0,0);                  // go home
  lcd.print("Motion Sensor");
  lcd.setCursor(0,1);
  lcd.print("Started"); 
  delay(3000);
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(runningLED,OUTPUT);
  pinMode(inputPin, INPUT);     // declare sensor as input
  runningLedTimer = millis();

  delay(3000);
  Serial.begin(9600);
}

void togglePowerLed ()
  {
   if (digitalRead (runningLED) == LOW)
      digitalWrite (runningLED, HIGH);
   else
      digitalWrite (runningLED, LOW);

  // remember when we toggled it
    runningLedTimer = millis(); 
  }  // end of toggle



void loop(){
 
  val = digitalRead(inputPin);      // read input value
 
  if ( (millis () - runningLedTimer) >= runningLedInterval)
     togglePowerLed();
 
  if (val == HIGH) {                // check if the input is HIGH
    digitalWrite(ledPin, HIGH);     // turn LED ON
    if (pirState == LOW) {
      lcd.setCursor(0, 1);
      lcd.print("                             ");
      // we have just turned on
      Serial.println("Motion detected!");
      lcd.setCursor(0, 1);
      lcd.print("Go away! ;..;");
      moCount++;
      // We only want to print on the output change, not state
      pirState = HIGH;

    }
  } else {
    digitalWrite(ledPin, LOW); // turn LED OFF
    if (pirState == HIGH){
      lcd.setCursor(0, 1);
      lcd.print("                             ");
      // we have just turned of
      Serial.println("Motion ended!");
      // We only want to print on the output change, not state
      pirState = LOW;
      lcd.setCursor(0, 1);
      lcd.print("No Motion. -.-");
    }
  }
  lcd.setCursor(0,0);
 // lcd.print("                              ");
 
  String mesg =  "Det. Count: ";
  String mesgComp = mesg + moCount;
  lcd.setCursor(0,0);
  lcd.print(mesgComp);
  lcd.setCursor(0,1);
  lcd.print(getTempHumAndPrint());
 
 
  delay (100);
}

String getTempHumAndPrint(){
  int T = sensor.readTemperature();
  int H = sensor.readHumidity();

  String mesg = "Temp: " + String(T) + " Hum: " + String(H);
  return mesg; 
