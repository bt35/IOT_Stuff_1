# Python 3 version... this does not work yet. 
# Bilbiopixel implemenation needs to be changed to Python3. 

import time, pytz, automationhat, Adafruit_DHT
from datetime import datetime
from astral import Astral
from bibliopixel.led import *
from BiblioPixelAnimations.strip import LarsonScanners
from bibliopixel.drivers.LPD8806 import *
from bibliopixel import LEDStrip
import bibliopixel.colors as colors
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
from multiprocessing import Process



app = Flask(__name__)

numLeds = 48
driver = DriverLPD8806(numLeds, c_order = ChannelOrder.BRG)
led = LEDStrip(driver)
anim = LarsonScanners.LarsonScanner(led, colors.Red )

# Processes 
@app.route("/")
def index():
    return render_template('index.html')

#background process happening without any refreshing
@app.route('/AC')
def AC():
    print ("Turning on AC")
    if automationhat.relay.two.is_on():
        automationhat.relay.three.off() ## Turn it off if it was on. 
        automationhat.relay.two.off()
        set_off() # Turn the LEDs off...
    else: # Turn it on... 
        set_blue()
        automationhat.relay.three.on() # Turn on the fan...
        automationhat.relay.two.on() # Turn on the compressor. 
    return "nothing"

@app.route('/Fan')
def fan():
    print ("Turning on Fan")
    if automationhat.relay.three.is_on():
        automationhat.relay.three.off() ## Turn it off if it was on. 
        set_off() # Turn the LEDs off...
    else: # Turn it on... 
        set_green()
        automationhat.relay.three.on() # Turn on the fan...
    return "nothing"

@app.route('/Heat')
def heat():
    p.start()
 #   r.start()

@app.route('/Off')
def off():
    all_off()


@app.route('/temp', methods= ['GET',  'POST'])
def temp():
    humidity, temperature = Adafruit_DHT.read_retry(11, 14)
    f = temperature * (9.0/5.0) + 32.0
    msg = "The Temperature is: {}, and humidity {}, and was polled a minute ago: {}".format (f,humidity,datetime.now().strftime("%I:%M%p on %B %d, %Y")) 
    print("Temperature: C:{} F:{}".format(temperature,f))
    #print (msg)
    return jsonify(msg)


# Turn it all off. 
def set_off():
    for i in range(numLeds):
        led.set(i,(0,0,0))
        led.update()

# Larson scanner for heat. 
#def set_red():
#    for i in range(numLeds):
#            led.set(i,(255,0,0))
#            led.update()
def set_red():
    anim.run()

# for fan
def set_green():
    for i in range(numLeds):
            led.set(i,(0,0,255))
            led.update()
# for cool
def set_blue():
    for i in range(numLeds):
            led.set(i,(0,255,0))
            led.update()


def run_heat():
    humidity, temperature = Adafruit_DHT.read_retry(11, 14)
    f = temperature * (9.0/5.0) + 32.0
    Hmessage = ""
    while True :
        humidity, temperature = Adafruit_DHT.read_retry(11, 14)
        f = temperature * (9.0/5.0) + 32.0
        if  f < 70 and automationhat.relay.one.is_off():
            print ("Turning on Heat")
            automationhat.relay.three.on() ## Turn it off if it was on. 
            automationhat.relay.one.on()
            set_red() # Turn the LEDs off...
            Hmessage = "Heat is on."
        elif f > 70 and automationhat.relay.one.is_on(): # Turn it off 
            set_off()
            automationhat.relay.three.off() # Turn on the fan...
            automationhat.relay.one.off() # Turn on the heat. 
            Hmessage = "Heat is off."
        print (f)
        print (temperature)
        time.sleep(60)
    return Hmessage

def all_off():
    automationhat.relay.three.off()
    automationhat.relay.one.off()
    automationhat.relay.two.off()
    set_off()
    p.terminate()
    #r.terminate()
    set_off()


p = Process(target=run_heat)
#r = Process(target=set_red)

if __name__ == "__main__":
    app.run(host= '0.0.0.0')
