from datetime import datetime
from bibliopixel.drivers.serial_driver import *
from bibliopixel import LEDStrip

import bibliopixel.colors as colors
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
from multiprocessing import Process

driver = DriverSerial(type=LEDTYPE.LPD8806, num=48)


led = LEDStrip(driver)
anim = Rainbows.RainbowCycle(led)
#anim = LarsonScanners.LarsonScanner(led, strip, colors.Red)

# Turn it all off. 
def set_off():
    for i in range(numLeds):
        led.set(i,(0,0,0))
        led.update()

# Larson scanner for heat. 
#def set_red():
#    for i in range(numLeds):
#            led.set(i,(255,0,0))
#            led.update()
def set_red():
    anim.run()

# for fan
def set_green():
    for i in range(numLeds):
            led.set(i,(0,0,255))
            led.update()
# for cool
def set_blue():
    for i in range(numLeds):
            led.set(i,(0,255,0))
            led.update()

def main():
    set_red()
main()
