# Basic Imports. Time to use for sleep. Pytz to use with astral.
# Automation Hat is a pimoroni module that handles the coms between the Pi and the 
# Automation Hat. 
# Adafruit_DHT is used for a DHT11 or DHT22 sensor. This project doesn't use either now, 
# as the temperature bounce on the DHT11 is too far. See READ me for more info.
# Serial is for reading information from an Arduino Uno to get temperatures and humidity 
# from an Adafruit Si7021 i2c sensor. The AutomationHat does not break out the SDA or SCL 
# pins. 
# Use threading to handle serial communication.. so we can use a while loop without blocking the 
# rest of the app.  
import time, pytz, automationhat, Adafruit_DHT, serial, threading
# Import date time. May not be needed, but I like to have it. 
from datetime import datetime
# Using Process to handle the heating loop. This may need to be removed in favor of another 
# thread class to handle state changes. 
from multiprocessing import Process
# Astral module ... maybe to use Geo location to check if night or not. 
# Use pytz and date time. 
from astral import Astral
# Bibliopixel stuff. These modules allow usage of a 48 LED LDP8806 strip to do colors. 
# LED Strip used to indicate what function the thermostat is running. 
# Not needed but fun, and the AutomationHat breaks out the MOSI, MISO, and SLCK 
# pins which means a LED strip should be easy to use. Maybe even other 
# LEDs can be used, like matrices. 
from bibliopixel.led import *
from BiblioPixelAnimations.strip import LarsonScanners
from bibliopixel.drivers.LPD8806 import *
from bibliopixel import LEDStrip
import bibliopixel.colors as colors

# Flask Stuff. 
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
app = Flask(__name__)

# Setting up LED stuff using Python2 version of bibliopixel. 
numLeds = 48
driver = DriverLPD8806(numLeds, c_order = ChannelOrder.BRG)
led = LEDStrip(driver)
anim = LarsonScanners.LarsonScanner(led, colors.Red )

# Old Global variables for temperatures. 
#f_temp = 0
#h = 0 

class Get_Serial_Info(object):
    """ 
	Get Temp and Humidity from Arduino Uno connected Via Serial 
        This class template was shamefully borrowed from a stack overflow question. 
    """
    def __init__(self, interval=1, com=serial.Serial('/dev/ttyACM0', 115200, timeout=.1)): 
        """ Constructor
        :type interval: int
        :param interval: Check interval, in seconds
        :type com: object
        :param arduino serial object.
        """
        self.interval = interval
        self.f_temp = 0 # Temp 
        self.h = 0      # Humidity 
        self.com = com  # com
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution

    def run(self):
        """ Read serial always and forever. Until app closes. """
        while True:
            # Read data from arduino device. 
            data = self.com.readline()[:-2] #the last bit gets rid of the new-line chars
            if data: 
                h, f = data.split("%") # Output read in from the arduino looks like 30%67. 
                                       # The arduino sketch returns readings in a single line with % between each reading. 
                                       # Split the two readings into a string for each reading. 
                self.h = int(h)        # Cast the humidity string to int. 
                self.f_temp = int(f)   # Cast the temp string to int. 
                print ("Hum: {} , Temp {}".format(self.h, self.f_temp)) # Print the output for testing. Remove when the 
                                                                        # appliation is daemonized.
            time.sleep(self.interval)


# Flask route to handle root dir, render index. 
@app.route("/")
def index():
    return render_template('index.html')

# Control the AC
@app.route('/AC')
def AC():
        print "Turning on AC"
        if automationhat.relay.two.is_on(): # If turn the AC off if it was on. Using the automation hat relay coms. 
            automationhat.relay.three.off() ## Turn it off if it was on. 
            automationhat.relay.two.off()
            set_off()                       # Turn off the LED strip. 
        else: # Turn it on... 
            set_blue()                      # Turn on the LED strip. Blue for cold. 
            automationhat.relay.three.on()  # Turn on the fan. No fan no air cirlculation. 
	    automationhat.relay.two.on()    # Turn on the compressor. No compressor no cold air. 
        return "nothing"                    # Return something. 

# Control just the fan. 
@app.route('/Fan')
def fan():
        print "Turning on Fan"
        if automationhat.relay.three.is_on(): # Turn the fan off.    
            automationhat.relay.three.off()   # Turn it off if it was on. 
            set_off()                         # Turn the LEDs off...
        else:                                 # Turn the fan on. 
            set_green()
            automationhat.relay.three.on()    # Turn the fan on. Using the fan relay. 
        return "nothing"                      # Return something. 

# Turn the LED off. Set no color to all of the 48 LEDs in the strip. 
def set_off():
    for i in range(numLeds):
        led.set(i,(0,0,0))
        led.update()

# Turn the LEDs red for heat. 
# Note the fan activates for heat but the set_green method is not called. 
def set_red():
    for i in range(numLeds):
        led.set(i,(255,0,0))
        led.update()

# Turn the LEDs green for fan. 
def set_green():
    for i in range(numLeds):
            led.set(i,(0,0,255))
            led.update()

# Turn the LEDs blue for cold. 
# Note that when the fan is paired with AC the set_green method is not called. 
def set_blue():
    for i in range(numLeds):
            led.set(i,(0,255,0))
            led.update()

# Flask route for heat. 
@app.route('/Heat')
def heat():
    p = Process(target=run_heat)  # Setup a process using multiprocessing for the heat cycle. 
    p.start()                     # Start the heat process. 
    return "Nothing"              # Return something. 

# Flask rouote for off. Turn off everything.
# Currently the off command does not terminate the p process. 
# Need to figure out how to spawn new heating processes once the originals have been killed.  
@app.route('/Off')
def off():
    all_off()        # Call the all_off function. 
    p.terminate()    # Attempt to terminate the heat process. 
    p.join()         # Reclaim the process. 
    return "Nothing" # Return something.

# ALL OFF. Turn relays off. 
def all_off():
    automationhat.relay.three.off()  # Turn off the fan.  
    automationhat.relay.one.off()    # Turn off the heater. 
    automationhat.relay.two.off()    # Turn off the AC.
    set_off()                        # Turn the LED strip off for good measure. 
    p.terminate()                    # Terminate a process too... this doesn't work. 
                                     # See the method above. 
    return "Nothing."                # Return something. 

# Handle the heat...
def run_heat():
    Hmessage = "" # string variable for return message. 
    while True :
        # Print the serial thread's temperature and humidity information. 
        print ("Hum: {} , Temp {}".format(t_h.h, t_h.f_temp))
        # Use the serial thread's temp info to check if its cold, 
        # then check to see if the heater relay is off. If yes turn on heat. 
        # If not cold and the relay is on turn it off. 
        # This logic needs work but has been neglected for process handling. 
        # We need a else statement to handle the Oops case.  
        if  t_h.f_temp < 70 and automationhat.relay.one.is_off():
            print "Turning on Heat"
            automationhat.relay.three.on() ## Turn it off if it was on. 
            automationhat.relay.one.on()
            set_red() # Turn the LEDs off...
            Hmessage = "Heat is on."
        elif t_h.f_temp > 70 and automationhat.relay.one.is_on(): # Turn it off 
            set_off()
            automationhat.relay.three.off() # Turn on the fan...
            automationhat.relay.one.off() # Turn on the heat. 
            Hmessage = "Heat is off."
        time.sleep(30) # We really don't need to poll this method that often. 
    return Hmessage

# Render temperature information to the flask app index page. 
@app.route('/temp', methods= ['GET',  'POST'])
def temp():
    msg = "The Temperature is: {}, and humidity {}, and was polled a minute ago: {}".format (t_h.f_temp,t_h.h,datetime.now().strftime("%I:%M%p on %B %d, %Y")) 
    return jsonify(msg)

# Do the main method.
if __name__ == "__main__":
    t_h = Get_Serial_Info()  # Start 
    app.run(host= '0.0.0.0') # Bind the flask app to active network interface. 
                             # Ideally this application is behind a firewall and the 
                             # 0.0.0.0 network is a local network. IE: 192.168.1 or 10.10.0.
                             # Do not put this on the internet unless you would like to be featured in 
                             # Hacker conference talk that references Shodan. 
