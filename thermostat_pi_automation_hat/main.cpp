#include "Adafruit_Si7021.h"

Adafruit_Si7021 sensor = Adafruit_Si7021();

void setup() {
  Serial.begin(115200);

  // wait for serial port to open
  while (!Serial) {
    delay(10);
  }
  
  if (!sensor.begin()) {
    Serial.println("Did not find Si7021 sensor!");
    while (true);
  }
}

void loop() {
 // char c_mesg[25];
  //char f_mesg[25];
 // char h_mesg[25];
  float c_f = sensor.readTemperature();
  float h_f = sensor.readHumidity();
  int c = c_f;
  int h = h_f;
  //dtostrf(c, 5, 2, c_mesg); 
  float f_t = (c * 9.0) / 5.0 + 32;
  int f = f_t;
  
  Serial.print(h);Serial.print("%");Serial.print(f);
  Serial.println();
  delay(5000);
}
