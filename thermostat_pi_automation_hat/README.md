# Raspberry Pi Flask Thermostat

I bought a Pimoroni AutomationHat while at a Microcenter... this is what I did with it. 

## How Does it work? 

NOTE: This project is currently still a work in progress... but it kinda works! 

This project uses a Pimoroni Automation hat to control a Central Air system using a standard Red, Green Yellow, White thermostat setup. The Pimoroni has three relays and each of these relas can control each of the Red, Green, Yellow thermostat functions. 

An Arduiono Uno and an Adafruit Si7021 temperature and humidity sensor are used to send temperature information to the raspberry pi via a USB cable. 

Python flask is used as the human interface with the raspberry pi. 
An LED is used to show which function the thermostat is currently running. 

## Materials

- Adafruit Temp & Humidity Module: Si7021
    - https://learn.adafruit.com/adafruit-si7021-temperature-plus-humidity-sensor/wiring-and-test
- Arduino Uno Board
- Raspberry Pi 2
- AutomationHat 
	- https://shop.pimoroni.com/products/automation-hat
- LED LDP8806 
	- https://www.adafruit.com/product/1948
	- Power for the LED. This project uses a 1M strip, use a 5v/2a power supply.
		- https://www.adafruit.com/product/276
	- Power supply adapter for connecting LED to the power.Something like this: 
			- https://www.adafruit.com/product/369
			- Please double check the adapter size. 
- Bell wire for creating jumpers. 
	- Some sort of copper wire. 

# Wiring:

Detailed Automation Hat Informationn: https://learn.pimoroni.com/tutorial/sandyj/getting-started-with-automation-hat-and-phat, also here: https://pinout.xyz/pinout/automation_hat 


## Relay Configuration 
We are using the Normally Open side of the Relays to keep the 24v signal from contacting the signal wires. 

24v, Red Cable is connected to Relay 3, in the NO slot. The red cable is then looped to the NO Slots on each of the other relays. This provides power for signal to the other relays. Without this no signal would be sent to the central air unit. 

Relay 3 is the Fan signal cable. This is the Green cable, which is connected to the COM slot ont the relay. 

Relay 2 is the AC signal calbe. This is the yellow cable and it is connected to the COM slot on the relay. 

Relay 1 is the furnace signal. This is the white cable and it is connected to the COM slot on the relay. 

Note, there is a breadboard with connection to a DHT11 sensor, it is not used currently. I left it wired anyway, and here is how: 

## LED Wiring 

Please follow the guide from Adafruit here for powering a 1M LED strip: https://learn.adafruit.com/digital-led-strip

In this project I use a DC jack to adapt the plug from a surge protector to the LED strip. The positive (power side) of the adapter is connected to the red cable of the LED strip and the negative (ground) side of the adapter is attached to a ground port on the AutomationHat. 

The pins comming from the LED strip itself connect to the breakout pins in the center of the Automation Hat as such: 

All of the below jumpers are mail to female to adapt the LED to the breakout on the automation hat. 
- Black, connected via a gray jumper to the ground. This means the LED strip itself is grouneded. The other ground above is for the power adapter. 
- Green, connected via a white jumper to he SCLK pin. 
- Yellow, connected via a blue jumper to the MOSI pin. 

### DHT 11 Sensor 

The red jumper is for 5V power from the automation hat connected to the power rail(+) on the  breadboard, and the ground is the green cable, it is connected to the ground(-) rail on the breadboard. The white cable runs from the DHT11 sensor to GPIO pin 14 on the automation hat. Use the Adafruit_DHT library to access informatoin from this sensor. 

Automation Hat Image with Wires: 
![RPI-WIREs](/thermostat_pi_automation_hat/pi_hat.jpg)

## Uno and Si7021 sensor wiring. 

The Arduino Uno is connected via a USB to the raspberry pi.

The Si7021 sensor is wired in the exact same way as this tutorial: https://learn.adafruit.com/adafruit-si7021-temperature-plus-humidity-sensor?view=all#assembly. 

The sketch is called main.cpp. 

![UNO-WIREs](/thermostat_pi_automation_hat/uno.jpg)


### Gif of Action

![alt_text](/thermostat_pi_automation_hat/output.gif)

## ToDo:

Fix threading.  

