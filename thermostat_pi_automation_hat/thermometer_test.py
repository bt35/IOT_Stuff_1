#!/usr/bin/python
from bibliopixel.led import *
from bibliopixel.animation import StripChannelTest
from bibliopixel.drivers.LPD8806 import *
from bibliopixel import LEDStrip
import bibliopixel.colors as colors
import Adafruit_DHT
import time



numLeds = 48
driver = DriverLPD8806(numLeds, c_order = ChannelOrder.BRG)
led = LEDStrip(driver)
#for i in range(numLeds):
#        led.set(i,(255,255,255))
#            led.update()

# get temp
while True:
    humidity, temperature = Adafruit_DHT.read_retry(11, 14)
    print (humidity, temperature)
    time.sleep(30)
    

