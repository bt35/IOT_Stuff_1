import time, pytz, automationhat, Adafruit_DHT
from datetime import datetime
from astral import Astral
from bibliopixel.led import *
from bibliopixel.animation import StripChannelTest
from bibliopixel.drivers.LPD8806 import *
from bibliopixel import LEDStrip
import bibliopixel.colors as colors


# Fan
# automationhat.relay.three.toggle()
# Heat 
# automationhat.relay.one.toggle()
# AC
# automationhat.relay.two.toggle()


# use this to get if night time...
a = Astral()
city = a['New York'] # Replace with your city
numLeds = 48
driver = DriverLPD8806(numLeds, c_order = ChannelOrder.BRG)
led = LEDStrip(driver)
#print("Temperature: C:{} F:{},Volts: {}, Value: {}".format(C,F,0 ,value))

def runThermo(f_temp):
    now = datetime.now(pytz.utc)
    sun = city.sun(date=now, local=True)

    # Check if Heat is on: 
    print (f_temp)
    # Winter? Run Heat: 
    if now >= sun['dusk'] or now <= sun['dawn']:
            print "It's dark outside"
            print automationhat.relay.one.is_off()
            if f_temp < 70 and automationhat.relay.one.is_off():
                automationhat.relay.one.on()
                print ("Heat on.")
    elif f_temp >= 70 and automationhat.relay.one.is_on():
            automationhat.relay.one.off()
            print ("Heat off.")


# for heat
def set_red():
    for i in range(numLeds):
            led.set(i,(255,0,0))
            led.update()
# for fan
def set_green():
    for i in range(numLeds):
            led.set(i,(0,255,0))
            led.update()
# for cool
def set_blue():
    for i in range(numLeds):
            led.set(i,(0,0,255))
            led.update()





while True:
    humidity, temperature = Adafruit_DHT.read_retry(11, 14)
    f = temperature * (9.0/5.0) + 32.0
    print("Temperature: C:{} F:{}".format(temperature,f))
    runThermo(f)
    time.sleep(600)



