#!/usr/bin/python

import sys
import Adafruit_DHT
import Adafruit_BBIO.ADC as ADC

sensor = Adafruit_DHT.DHT11

pin = "P8_11" # Temp sensor pin out

ADC.setup()

#Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
   
# Un-comment the line below to convert the temperature to Fahrenheit.
temperature = temperature * 9/5.0 + 32
light = ADC.read("P9_40") # light sensor pin out
soil = ADC.read("P9_39") # soil sensor pin out
    
mapped_soil = int(round(soil * 100)):
#mapped_soil = map(soil, 0,4095,0,100)
  
# Note that sometimes you won't get a reading and
# the results will be null (because Linux can't
# guarantee the timing of calls to read the sensor).
# If this happens try again!
if humidity is not None and temperature is not None:
    print('The temperature, humidity, light level and soil moisture percentage of the kitchen:')
    print('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity))
    print('Light Sensor: {} Soil Sensor: {}%'.format(light, mapped_soil))
else:
    print('Failed to get reading. Try again!')
    sys.exit(1)

