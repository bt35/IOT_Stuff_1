# Beagle Bone Black Soil Moisture Sensor

## What is it? 
The purpose of this project is to monitor, currently, one Heart Shaped Philodendron plant to ensure that it maintains roughly 70% to 90% soil moisture. Since all of my plants are in the same area of the house they should receive the same amount of light and if they are all watered at roughly the same time they should have the same amount of moisture in the soil. 

## Materials 
- Beagle Bone Black Wireless 
- Funduino Soil Sensor (SKU:SE N0114)
- DHT11 Temperature Sensor from Arduino Sense Kit
- Photo Resistor Light Sensor (KY-018)
- Bread Board
- The case that one of my Arduino projects came in. 
- Jumpers - By now you should have a large bag of them...
![alt text](/PlantWatcher/20170627_023137.jpg)

## Pin Outs 
- Temperature Sensor: P8_11
- Light Sensor: P9_40 
- Soil Sensor: P9_39

## The How
This project is very hacky. In that there is no centralized mailing system because I put this project together well before I googled smtplib for Python. Also, I am used to using postfix or some other MTA to handle all of my mail needs. 

```
Beagle Bone Cron Jobs: 
0 19 * * * /home/me/just_checking.py > /var/www/html/just_checking_afternoon.txt
0 7 * * * /home/me/just_checking.py > /var/www/html/just_checking_morning.txt

```
So with the above in mind... The code on the beagle bone reads the sensors twice a day using a cron job stored in the Beagle Bone Black's root crontab. The code will read the senors and then write the output to the Beagle Bone Black's built in Apache server. Then on another system there are yet two more cron jobs, one in the morning and one in the after noon, that read the text file into an email. This other system has a fully loaded MTA and that provides the email notifications. 

```
Mail Server Crontab
5 19 * * * /usr/bin/curl http://10.0.0.27:8080/just_checking_afternoon.txt | mailx -s "Update on the Plants" xxxxx@gmail.com
5 7 * * * /usr/bin/curl http://10.0.0.27:8080/just_checking_morning.txt | mailx -s "Update on the Plants" xxxxx@gmail.com
```

## Images

![alt text](/PlantWatcher/20170627_022646.jpg)

![alt text](/PlantWatcher/20170627_022652.jpg)

![alt text](/PlantWatcher/20170627_022657.jpg)


