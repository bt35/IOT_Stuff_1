# Beagle Bone Black Washing Machine Email Monitor

## Attributions 
This code is an adaptation of MakerBee's instructables tutorial, which can be found here: http://www.instructables.com/id/Washer-Dryer-Laundry-Alarm-using-Arudino-SMS-Text-/
- Creative Commons: https://creativecommons.org/licenses/by-nc-sa/2.5/
- The changes are detailed below and in this code file. Most noteable differences are the microcontroller used, and the accelerometer.

## What is it? 
This project is an effort to ensure that I do not have to walk down the stairs several times during a washer cycle. So this project was born out of laziness, an extra beagle bone black, and a sparkfun ADXL335 accelerometer. 

## Materials 
- Beagle Bone Black Wireless - Running Debian Jessie 
- ADXL335 Spark Fun 3 Axis Tilt Accelerometer 
- Beagle Bone Case 
- Breadboard (Solderless Connections with Sticky backing)
- 5 Jumpers. * See Notes. 
- USB Switching Power Supply (Non adaptive phone charger will suffice) 

## Accelerometer Pinout connections for Beagle Bone Black 
- Ground is connected to P9_2 
- VCC is connected to P9_3 
- X axis analog output is connected to P9_33 (AKA: AIN4)
- Y axis analog output is connected to P9_35 (AKA: AIN6)
- Z axis analog output is connected to P9_36 (AKA: AIN5)

## Notes: 
The Z axis is connected to the Beagle Bone Black however it is not necessary as that axis is not required to detect movement. Therefore only 4 jumpers are required. All stable measurements taken by running print statements on x and y analog output on a flat solid structure. 

## Images

![alt text](/BeagleBoneBlack_Laundry_Notifier/20170627_020547.jpg)

![alt text](/BeagleBoneBlack_Laundry_Notifier/20170627_020551.jpg)
