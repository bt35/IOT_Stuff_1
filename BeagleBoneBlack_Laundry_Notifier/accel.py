# This code is an adaptation of MakerBee's instructables tutorial  
# which can be found here:
#   http://www.instructables.com/id/Washer-Dryer-Laundry-Alarm-using-Arudino-SMS-Text-/
# Creative Commons: https://creativecommons.org/licenses/by-nc-sa/2.5/
# The changes are detailed below and in this code file. Most notable differences are
# the micro-controller used, and the accelerometer. 
#
# Materials used: 
#   Beagle Bone Black Wireless - Running Debian Jessie 
#   ADXL335 Spark Fun 3 Axis Tilt Accelerometer 
#   Beagle Bone Case 
#   Breadboard (Solder-less Connections with Sticky backing)
#   5 Jumpers. * See Notes. 
#   USB Switching Power Supply (Non adaptive phone charger will suffice)
#
#Accelerometer Pin-out connections for Beagle Bone Black 
#   Ground is connected to P9_2 
#   VCC is connected to P9_3 
#   X axis analog output is connected to P9_33 (AKA: AIN4)
#   Y axis analog output is connected to P9_35 (AKA: AIN6)
#   Z axis analog output is connected to P9_36 (AKA: AIN5)
#
#Notes: 
# The Z access is connected to the Beagle Bone Black however 
# it is not necessary as that axis is not required to detect movement. 
# Therefore only 4 jumpers are required. 
# All stable measurements taken by running print statements on x and y analog 
# output on a flat solid structure. 

# Imports. 
# Adafruit Beagle Bone Black ADC module. 
import Adafruit_BBIO.ADC as ADC

# Import math for the absolute value function. 
# and time for the sleep function. 
import time
import math

# Import email libraries for use
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage

# Instantiate the ADC module. 
ADC.setup()

# Initialize variables for x and y pins 
# These will be used as global. 
# If the z access is to be read uncomment it, 
# and its related print statements further down 
# in the code. 

x = ADC.read("P9_33")
y = ADC.read("P9_35")
#z = ADC.read("P9_36")

# Initialize two variables for the last x and y inputs. 
# These variables are set to 0.9 as I had problems with 
# zero division exceptions. So the first setting of the 
# variables are set as close as possible to an actual x
# and y measurement without movement. IE: On a flat stable 
# surface. 

x_Last = 0.9
y_Last = 0.9

# Set sensitivity to be 0.019. This number is just one one thousandths
# greater than the stable measurements of x and y. 
sensitivityX = sensitivityY = (0.020)

# Counter for movement
counter = 0
waitTime = 4

# Setter function for x and y. 
def set_xYZ():
    global x, y
    x = ADC.read("P9_33")
    y = ADC.read("P9_35")

# This function does the work. 
def get_xYZ():
    # declare global variables to use. 
    global x, y, z, x_Last, y_Last, sensitivityX, sensitivityY, counter
    if (counter < (waitTime * 3000)):
        # read x and y values. 
        set_xYZ()    

        # calculate the change in x and y
        changeX = x_Last - x
        changeY = y_Last - y

        # calculate the change in x and y as a
        # percentage.
        percentX = (math.fabs(changeX / x_Last) )
        percentY = (math.fabs(changeY / y_Last) )
    
        # Commented out print statemnts used for testing. 
        #if (x >= .92):
        #    print 'x = {0} \n'.format(x)
        #    print 'y = {0} \n'.format(y)
        #    print 'z = {0} \n'.format(z)
   
        #print 'percent x = {0}\n'.format(percentX)
        #print 'percent y = {0}\n'.format(percentY)
 
        # Check if the beaglebone is moving by comparing the 
        # percentage of change in x and y against the sensitivity. 
        # If the change in x and y is less than the sensitivity 
        # there is no movement, else there is movement. 
        if (percentX < sensitivityX and percentY < sensitivityY):
            print "NO MOVEMENT\n"
            counter = counter + 1;
        else:
            print "MOVEMENT\n"
            counter = 0
 
        x_last = ADC.read("P9_33")
        y_last = ADC.read("P9_35")
    else:
        send_email()
        counter = 0  #Reset the counter so we don't spam ourselves. 
#         print ""
    print "Counter = {0}\n".format(counter)

def send_email():
    msg = MIMEMultipart()
    msg['Subject'] = 'Laundry Monitor Message'
    msg['From'] = 'xxxxxxxxxx@mail.cc'
    msg['To'] = 'xxxxxxxxxxx@mail.cc'

    text = MIMEText("Laundry is Done")
    msg.attach(text)

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    my_email = "xxxx@gmail.com"
    my_password = "xxxxxxxxxxx"
    destination = "xxxxx@gmail.com"

    server.login(my_email, my_password)
    server.sendmail(my_email, destination, msg.as_string())
    server.quit()
    print("Your email has been sent!") #debugging

while True:
   get_xYZ()
   time.sleep(.02)

