# IOT_Stuff
Raspberry Pi, Arduino and BeagleBone Black Stuff 

## Herein
Contains some of the IOT projects that I work on in my spare time due to boredom.

## Projects
- Beagle Bone Black Wireless Washing Machine Monitor. 
- Beagle Bone Black Wireless Plant Moisture Sensor
- Raspberry Pi Touch Screen Hacky Security System
- Rasbperry Pi, AutomationHat, and Arduino Uno thermostat Flask Project. 
- Arduino Motion Sensing LED using Mosfet. 
