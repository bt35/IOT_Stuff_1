#define PIN 11      // control pin
#define DELAY 10    // 20ms internal delay; increase for slower fades

int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0; 

void setup() {
  off();
  pinMode(PIN, OUTPUT);
  pinMode(inputPin, INPUT);     // declare sensor as input
 
 // Serial.begin(9600);
}

void loop() {
  val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    if (pirState == LOW) {
      pirState = HIGH;
      on();
    }
  } else {
    if (pirState == HIGH){
      pirState = LOW;
      off();
    }
  }
  delay(5);
}


void on(){
  for(int i=0; i<255; i++) {
    analogWrite(PIN, i);
    delay(DELAY);
  }
}

void off(){
  analogWrite(PIN,0);
}

