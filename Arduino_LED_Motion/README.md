# What's This? 

It's a Motion Sensing LED strip. 

## Why? 

Well, it is a really sketch prototype for staircase lights. The goal of the project is to create a motion sensing LED stair case. 

## Materials
- Arduino Uno. But any Arduino should work. This project should not need any internet capability and a teensy or a Trinket could probably do the job. - Jumper wires. These jumper wires should be come solder connections at some point. 
- 4 Pin White LED Strip. These are commonly called cabinet LEDs. 
 - This LED strip might work: https://www.adafruit.com/product/887?length=1
- 12 Volt 1 amp power supply. 
 -https://www.adafruit.com/product/798
- DC Power Adapter. 
 -https://www.adafruit.com/product/368
- Plastic Box and Push Pins
- PIR Motion Sensor
- Electrical Tape

## Pin Out

For the pin out follow this guide: http://yaab-arduino.blogspot.com/2015/01/driving-led-strip-with-arduino.html, thanks to author whoever you are.

Another good tutorial is: https://learn.adafruit.com/rgb-led-strips/wiring 

## Code

Note the code from the above tutorial will work on its own using the same pinout and setup. The code contained in this repository utilizes a motion sensor, and minimal fading. 

## Warning: 

Some electrical tape around the exposed jumper pins on the outside of the plastic case to isolate the ground and power pin on the LED connection. 

## Images 
![alt text](/Arduino_LED_Motion/proj.jpg)

### Gif of Action
![alt_text](/Arduino_LED_Motion/output.gif)


